'use strict';

// Import mongoose lib
const mongoose = require('mongoose');


module.exports = () => {
	// Connecting and authenticating with database
	mongoose.connect('mongodb://h1r1n:Hermione934-@ds235711.mlab.com:35711/zambon')
		.then(data => {
			console.log('We have a connection with MongoDB');
		})
		.catch(error => {
			console.error('Houston, we got a database problem');
			console.error(error);
		});

}
