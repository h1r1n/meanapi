'use strict';
const Product = require('mongoose').model('Product');


exports.getProducts = (req, res, next) => {
    Product.find()
    .select('_id name image market package price unit_margin')
    .populate('market')
    .then(products => res.json(products))
    .catch(error => next(error));
}

exports.addProduct = (req, res, next) => {

    const product = new Product(req.body);

    product.save()
        .then(() => res.json(product))
        .catch((error) => next(res.status(401).json(error)));
};

exports.deleteProduct = (req, res, next) => {
    Product.remove({
        _id: req.params.product_id
    }, function (err, product) {
        if (err) {
            res.send(err);
        }
        res.json('Product was successfully deleted');
    });
}