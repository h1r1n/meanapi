'use strict';
const User = require('mongoose').model('User');


exports.me = (req, res, next) => {
    res.json(req.user);
}

exports.getUsers = (req, res, next) => {
    User.find()
    .select('id username displayName email imageURL active')
    .lean()
    .then(users => res.json(users))
    .catch(error => next(error));
}

exports.deleteUser = (req, res, next) => {
    User.remove({
        _id: req.params.user_id
    }, function(err, user){
        if(err)  {
            res.send(err);
        } 
        res.json('User was successfully deleted') 
    }
    );
}