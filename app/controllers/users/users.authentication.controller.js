'use strict';

const mongoose = require('mongoose'),
    passport = require('passport'),
    jwt = require('jwt-simple'),
    User = require('mongoose').model('User');


exports.signin = (req, res, next) => {
    passport.authenticate('local', { session: true }, (err, user) => {
        if (err) {
            return res.status(500).json(err)
        }
        req.login(user, { session: false }, (err) => {

            if (!user || err) return res.status(401).json({ error: err || 'Invalid Username or password' });

            const expireTime = Date.now() + (168 * 60 * 60 * 1000); // A week from now (168 hrs)
            // const expireTime = Date.now() + (60 * 60 * 1000); // An Hour
            // const expireTime = Date.now() + (60 * 1000); // A Minute

            // generate login token
            const tokenPayload = {
                id: user.id,
                exp: expireTime
            };

            let token = jwt.encode(tokenPayload, 'secret');
            user = user.toJSON();
            delete user.password;

            res.json({user:user, token:token});
        });

    })(req, res, console.log);
};

exports.signup = (req, res, next) => {

    const user = new User(req.body);

    user.save()
        .then(() => res.json(user))
        .catch((error) => next(res.status(401).json(error)) );
};