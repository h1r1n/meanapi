'use strict';
const Market = require('mongoose').model('Market');


exports.getMarkets = (req, res, next) => {
  Market.find()
    .select('_id name slug icon')
    .lean()
    .then(markets => res.json(markets))
    .catch(error => next(error));
}

exports.addMarket = (req, res, next) => {

  const market = new Market(req.body);

  market.save()
      .then(() => res.json(market))
      .catch((error) => next(res.status(401).json(error)) );
};

exports.deleteMarket = (req, res, next) => {
  Market.remove({
      _id: req.params.market_id
  }, function(err, market){
      if(err)  {
          res.send(err);
      } 
      res.json('Market was successfully deleted'); 
  }
  );
}