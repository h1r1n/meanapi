const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const MarketShema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    slug: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    icon: {
        type: String,
    },
    products: [{
        type: Schema.Types.ObjectId, 
        ref: 'Product'
    }],
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    }
});



// We do not want to have __v property in plain objects and json
MarketShema.set('toJSON', { getters: true, virtuals: true, versionKey: false });
MarketShema.set('toObject', { getters: true, virtuals: true, versionKey: false });


// Register schema
mongoose.model('Market', MarketShema);