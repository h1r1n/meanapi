const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const ProductShema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    market: {
        type: Schema.Types.ObjectId, 
        ref: 'Market'
    },
    package: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    unit_margin: {
        type: Number,
        required: true
    },
    updated: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    }
});



// We do not want to have __v property in plain objects and json
ProductShema.set('toJSON', { getters: true, virtuals: true, versionKey: false });
ProductShema.set('toObject', { getters: true, virtuals: true, versionKey: false });


// Register schema
mongoose.model('Product', ProductShema);