'use strict';

const productController = require('../controllers/product/product.controller');
const passport = require('passport');

module.exports = (app) => {
    app.get('/products', productController.getProducts);
    app.post('/products/add', passport.authenticate('jwt', { session: false }), productController.addProduct);
    app.delete('/products/delete/:product_id', passport.authenticate('jwt', { session: false }), productController.deleteProduct);
}