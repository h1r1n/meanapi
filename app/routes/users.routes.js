'use strict';

const usersAuth = require('../controllers/users/users.authentication.controller');
const users = require('../controllers/users/users.controller');
const passport = require('passport');

module.exports = (app) => {
    app.get('/me', passport.authenticate('jwt', { session: false }), users.me);
    app.get('/users', passport.authenticate('jwt', { session: false }), users.getUsers);
    app.delete('/users/delete/:user_id', passport.authenticate('jwt', { session: false }), users.deleteUser);
    app.post('/signin', usersAuth.signin);
    app.post('/signup', usersAuth.signup);
}