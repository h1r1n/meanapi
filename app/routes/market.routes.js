'use strict';

const marketController = require('../controllers/market/market.controller');
const passport = require('passport');

module.exports = (app) => {
    app.get('/markets', marketController.getMarkets);
    app.post('/markets/add', passport.authenticate('jwt', { session: false }), marketController.addMarket);
    app.delete('/markets/delete/:market_id', passport.authenticate('jwt', { session: false }), marketController.deleteMarket);
}